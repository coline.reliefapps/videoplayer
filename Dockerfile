FROM node:8.11.2-alpine as node
WORKDIR /usr/src/app
RUN apk add --no-cache git
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
RUN npm install @angular/cli