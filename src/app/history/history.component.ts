import { Component, OnInit } from '@angular/core';
import { VideoService } from '../video.service'
import { Observable } from 'rxjs'

@Component({
  selector: 'history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})

export class HistoryComponent implements OnInit {

  history$: Observable<Array<object>>;

  constructor(private videoService: VideoService) { }

  sendLink(link) {
    this.videoService.setUrl(link.url)
  }

  ngOnInit() {
    this.history$ = this.videoService.getHistory()
  }
}
