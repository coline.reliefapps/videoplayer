import { Component, OnInit } from '@angular/core';
import { VideoService } from '../video.service'
import { Observable } from 'rxjs'
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';


@Component({
  selector: 'video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.scss']
})
export class VideoViewComponent implements OnInit {
  
  link$: BehaviorSubject<string>;

  constructor(private videoService: VideoService) { }

  sendToBookmarks() {
    this.link$.subscribe(link => {
      this.videoService.addToBookmarks(link.replace('embed/', 'watch?v='))
    })
  }
  
  ngOnInit() {
    this.link$ = this.videoService.getUrl()
  }
}
