import { Component, OnInit } from '@angular/core';
import { VideoService } from '../video.service'

@Component({
  selector: 'search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  selectedLink: string
  error: string
  
  constructor(private videoService: VideoService) { }

  onSelect(link: string): void {
    this.selectedLink = link;
  }

  sendLink() {   
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
    var validYoutubeUrl = this.selectedLink.match(regExp);
    if (validYoutubeUrl && validYoutubeUrl[2].length == 11) {
      this.error = ''
    } else {
      this.error = 'Not a valid youtube url'
    }
    this.videoService.setUrl(this.selectedLink)
  }

  ngOnInit() {
  }

}
