import { Component, OnInit } from '@angular/core';
import { VideoService } from '../video.service'
import { Observable } from 'rxjs'

@Component({
  selector: 'bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss']
})

export class BookmarksComponent implements OnInit {

  bookmarks$: Observable<Array<object>>;

  constructor(private videoService: VideoService) { }

  sendLink(link) {
    this.videoService.setUrl(link.url)
  }

  ngOnInit() {
    this.bookmarks$ = this.videoService.getBookmarks()
  }
}
