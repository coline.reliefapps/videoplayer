import { Component, PipeTransform, Pipe, OnInit } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

@Pipe({ name: 'toDate' })
export class ToDatePipe implements PipeTransform {
  constructor() {}
  transform(date) {
    var rx = /(\d\d\d\d[-]\d\d[-]\d\d)/g;
    var match = rx.exec(date);
    return match[1]; 
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() { }
  title = 'Youtube Video Player';
}
