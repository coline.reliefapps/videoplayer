import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Video } from './video';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  private videoUrl = new BehaviorSubject<string>('');
  private history = new BehaviorSubject<Video[]>([]);
  private bookmarks = new BehaviorSubject<Video[]>([])

  constructor(private http: HttpClient,) { }

  getUrl(): BehaviorSubject<string> {
    return this.videoUrl
  }

  getHistory(): BehaviorSubject<Video[]> {
    this.http.get<Video[]>('http://localhost:8000/history')
      .subscribe(history => {
        this.history.next(history)
        localStorage.setItem('history', JSON.stringify(history));
      }, error => {
        this.history.next(JSON.parse(localStorage.getItem('history')))
      })
    return this.history
  }

  getBookmarks(): BehaviorSubject<Video[]> {
    this.http.get<Video[]>('http://localhost:8000/bookmarks')
      .subscribe(bookmarks => {
          this.bookmarks.next(bookmarks)
          localStorage.setItem('bookmarks', JSON.stringify(bookmarks))
      }, error => { 
        this.bookmarks.next(JSON.parse(localStorage.getItem('bookmarks')))
      })
    return this.bookmarks
  }

  setUrl(videoUrl: string): void {
    this.videoUrl.next(videoUrl.replace('watch?v=','embed/'))
    this.http.post('http://localhost:8000/history', {url: videoUrl}).subscribe(data => {
      let history = this.history.getValue()
      history.unshift({ id: data['id'], name: data['name'], created: data['created'], url: data['url'] })
      this.history.next(history)
    });
  }

  addToBookmarks(videoUrl: string) {
    this.http.post('http://localhost:8000/bookmarks', {url: videoUrl}).subscribe(data => {
      let bookmarks = this.bookmarks.getValue()
      let alreadyExist = false
      bookmarks.forEach(bookmark => {
        if (bookmark['url'] === data['url']) {
          alreadyExist = true
        }
      })
      if (alreadyExist === false) {
        bookmarks.unshift({ id: data['id'], name: data['name'], created: data['created'], url: data['url'] })
        this.bookmarks.next(bookmarks)
      }
    });
  }
}
