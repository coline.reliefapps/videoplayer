#!/bin/bash

set -e

# Remove javascript files
REMOTE_PATH=/var/www/html/newbies/coline
OUT=./dist/videoPlayer3
CONNECTION=tester@217.70.189.97

echo -e "cleaning destination ..."
CMD="mkdir -p ${REMOTE_PATH} && cd ${REMOTE_PATH} && rm -rf *"
ssh -oStrictHostKeyChecking=no -o PubkeyAuthentication=yes $CONNECTION "$CMD"

echo -e "synchronizing files ..."
scp -o stricthostkeychecking=no -o PubkeyAuthentication=yes -r $OUT/* $CONNECTION:$REMOTE_PATH

echo -e "Deployed !!"
